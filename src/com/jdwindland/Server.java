package com.jdwindland;

import java.io.*;
import java.net.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.*;

public class Server {

    //Method to start the HTTP server
    public static HttpServer serverStart() throws IOException, IllegalArgumentException, NullPointerException {
        System.out.println("\nThe server is started.\n");
        HttpServer server = HttpServer.create(new InetSocketAddress(8500), 0);
        HttpContext context = server.createContext("/");
        context.setHandler(Server::getObject);
        server.start();
        return server;
    }

    //Method to get person object and sent it to HTTP server
    public static void getObject(HttpExchange exchange) throws IOException {
        Person person = Client.newPerson();
        String response = Server.toJSON(person);

        exchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    //Method to convert person object to JSON string
    public static String toJSON(Person person) throws JsonProcessingException{
        ObjectMapper mapper = new ObjectMapper();
        String string;
        string = mapper.writeValueAsString(person);
        return string;
    }

    //Method to stop server and close connections
    public static String serverStop(HttpServer server) throws IllegalArgumentException{
        server.stop(0);
        return "The server has been stopped.";
    }
}
