package com.jdwindland;

import java.lang.*;

public class Person {

    //Define person attributes
    private String name;
    private char gender;
    private int age;
    private String address;

    //Methods that can return the value for each attribute
    public String getName(){
        return name;
    }

    public char getGender(){
        return gender;
    }

    public int getAge(){
        return age;
    }

    public String getAddress(){
        return address;
    }

    //Methods to set the value for each attribute
    public void setName(String name){
        this.name = name;
    }

    public void setGender(char gender){
        this.gender = gender;
    }

    public void setAge(int age){
        this.age = age;
    }

    public void setAddress(String address){
        this.address = address;
    }

    //Method that can be called to display the object
    @Override
    public String toString() {
        return  "\tName: " + name +
                "\n\tGender: " + gender +
                "\n\tAge: " + age +
                "\n\tAddress: " + address;
    }
}

